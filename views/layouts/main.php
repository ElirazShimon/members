<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
	//NavBar::begin([                                             //  מה שהיה במקורי לא משנה אם אני מחוברת או לא, אם הייתי לוחצת על הסימפל הייתי מגיעה  לדף הבית
       // 'brandLabel' => 'My Company',                            //מה שהיה במקורי
      //  'brandUrl' => Yii::$app->homeUrl,                        //מה שהיה במקורי
     Yii::$app->user->isGuest ? $brandUrl = ['site/index'] :         //בודק האם המשתמש מחובר, אם לא, בלחיצה על הכפתור של הסימפל יציג את דף הבית של האתר
		$brandUrl = Yii::$app->homeUrl;                               //אם כן יציג את דך הבית של המערכת
	NavBar::begin([
		'brandLabel' => 'SimpleCRM',
		'brandUrl' => $brandUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            //['label' => 'Home', 'url' => ['/site/index']], //מה שהיה במקורי
           // ['label' => 'About', 'url' => ['/site/about']],   //מה שהיה במקורי
            //['label' => 'Contact', 'url' => ['/site/contact']],   //מה שהיה במקורי
			
			Yii::$app->user->isGuest ?  //אם המשתמש הוא אורח
			['label' => 'Home', 'url' => ['/site/index']]:
			
			Yii::$app->user->isGuest ?  //אם המשתמש הוא אורח
			['label' => 'About', 'url' => ['/site/about']]:
			
			Yii::$app->user->isGuest ?   //אם המשתמש הוא אורח
			['label' => 'Contact', 'url' => ['/site/contact']]:
			
			['label' => 'Leads', 'url' => ['/lead/index']],
			['label' => 'Users', 'url' => ['/user/index']],
            
			'',
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
