<?php


 
namespace app\models;

use Yii;
use \yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
 

		public $role; 
		
		public static function tableName()
		{

			return 'user';
		}
		public function rules()
		 {
			return [
						[['username', 'password', 'auth_key', ], 'string', 'max' => 255],
			[['firstname', 'lastname', 'phone', ], 'string', 'max' => 255],
			['email', 'email'],
			['role', 'safe'],
			[['username', 'password', ], 'required'],
			[['created_at', 'updated_at', ], 'integer'],
			[['created_by', 'updated_by', ], 'integer'],
            [['username'], 'unique'],			
			];
		}
 

		public function attributeLabels()
		{
			return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
			'firstname' => 'First name',
			'lastname'  => 'Last name',
			'email'  => 'Email',
			'phone'  => 'Phone number',
			'role'  => 'User role',
			];
		}	
		public function behaviors()  // פונקציה שקשורה ל4 שדות התחקור
		{
			return 
			[
				[
					'class' => BlameableBehavior::className(),
					'createdByAttribute' => 'created_by',
					'updatedByAttribute' => 'updated_by',
				],
					'timestamp' => [
					'class' => 'yii\behaviors\TimestampBehavior',
					'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
					],
				],
			];
		}
		
		public function getUserole() //פונקציה שקשורה לטבלת השמת התפקידים למשתמשים
		{
			$roleArray = Yii::$app->authManager->
					getRolesByUser($this->id);
			$role = array_keys($roleArray)[0];				
			return	$role;
		}
		
		public static function getRoles()   //פונקציה שקשורה לטבלת השמת התפקידים למשתמשים
		{
			$rolesObjects = Yii::$app->authManager->getRoles();
			$roles = [];
			foreach($rolesObjects as $id =>$rolObj){
				$roles[$id] = $rolObj->name; 
			}
			return $roles; 	
		}
		
		public function afterSave($insert,$changedAttributes) //פונקציה שקשורה לטבלת השמת התפקידים למשתמשים
    	{
			$return = parent::afterSave($insert, $changedAttributes);

			$auth = Yii::$app->authManager;
				$roleName = $this->role; 
				$role = $auth->getRole($roleName);
				if (\Yii::$app->authManager->getRolesByUser($this->id) == null)
				{
						$auth->assign($role, $this->id);
				} 
				else {
						$db = \Yii::$app->db;
						$db->createCommand()->delete('auth_assignment',
								['user_id' => $this->id])->execute();
						$auth->assign($role, $this->id);
					 }

       	 return $return;
  	 }		
			
		
		
		public function getFullname()  //פונקציה שנותנת את השם המלא שמורכבת משתי תכונות
		{
			return $this->firstname.' '.$this->lastname;
		}
		
		public static function getUsers() //פונקציה שמחזירה מערך עם כל שמות היוזרים
		{
			$allUsers = self::find()->all();
			$usersFirstname = ArrayHelper::
						map($allUsers, 'id', 'firstname');
			$usersLastname = ArrayHelper::
						map($allUsers, 'id', 'lastname');
			$users = []; 
			foreach($usersFirstname as $id => $firstname) 
			{

				$users[$id] = $firstname.' '.$usersLastname[$id]; 
			}
						
			return $users;						
		}
		
		//אפשר במקום לרשום את הפונקציה שלעיל, לרשום את הפונקציה הבאה, היא שקולה
		//public static function getUsers()
		//{
		//	$users = ArrayHelper::
		//			map(self::find()->all(), 'id', 'fullname');
		//	return $users;						
		//}
		
		public static function findIdentity($id)   //כל הפונקציות הבאות קשורות לאינטרפייס שמודל יוזר מיישם
		{
			return static::findOne($id);
		}
 

		public static function findByUsername($username)
		{
			return static::findOne(['username' => $username]);
		}

	

		public static function findIdentityByAccessToken($token, $type = null)
		{

			throw new NotSupportedException('You can only login
							by username/password pair for now.');
		}
		 public function getId()
		{
			return $this->id;
		}
 
 
		public function getAuthKey()
		{

			return $this->auth_key;
		}
 

		public function validateAuthKey($authKey)
		{

			return $this->getAuthKey() === $authKey;
		}	
 
		
		public function validatePassword($password)
		{
			return Yii::$app->security->validatePassword($password, $this->password);
		}

		public function beforeSave($insert)
		{

			$return = parent::beforeSave($insert);

			if ($this->isAttributeChanged('password'))
			{
					$this->password = Yii::$app->security->generatePasswordHash($this->password);
			}
			if ($this->isNewRecord)
			{
					$this->auth_key = Yii::$app->security->generateRandomString(32);
			}
			return $return;
		}
	
}